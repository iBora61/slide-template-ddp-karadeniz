---
title: Concept presentation Progress Tracking
description: Presentation of our concept for ddp course
author: Jaime Joel Romero Mozota, Bora Karadeniz 
keywords: marp,marp-cli,slide
url: Please replace this with the URL of the deployed presentation
marp: true
image: https://j6f3f9w3.rocketcdn.me/wp-content/uploads/2020/08/Implementieren-Prozessanalyse.png
---

# Concept presentation

## Progress tracking  

By **Jaime Joel Romero Mozota** and **Bora Karadeniz**

---

## Collected stories

"We don't know what the state of the work is" from contruction manager👷‍♂️ sitting in the container 

---
## Mindmaps


```mermaid
flowchart LR
  id1(Management of safety and real time Processes) --> Tracking
  Tracking --> id2[(Progress Monitoring)] & Hazards & Materials & Worker
```  

``` mermaid

  flowchart LR
  id1(Automated Progress Monitoring) --> Approach & Benefits 
  Approach --> id2(Laserscanner) --> LiDAR & Kinect --> id3(Point Clouds) --> id4(Component Recognition) --> id5[(Real-Time 3D BIM Model)]
  Benefits --> id9[(Site-Independent Monitoring)] & id10[(Real-Time Monitoring)] & id11[(Automated Documentation)]

```

---

## Concept

Our goal is to record the current construction progress and construction status by creating point clouds and transfer the individual construction elements into a 3D BIM model and visualise them using object recognition. 

This will solve the problem that the finished work can only be checked sporadically by the site managers and foremen , so the schedule can be kept.  

---

## Useful tools 

Transform point clouds in 3d objects:
- [Point Cloud Library](https://github.com/PointCloudLibrary/pcl)
- [Meshlab](https://github.com/cnr-isti-vclab/meshlab)
Bim visualizer:
- [xeokit bim viewer](https://github.com/xeokit/xeokit-bim-viewer)
Point cloud and 3d  object comparer:
- [Point cloud comparer](https://github.com/CloudCompare/CloudCompare)

---

## Market research

- [Strabag point cloud research ](https://innovation.strabag.com/projekt/digital-in-die-tiefe-2/)

- [Gilbane building company](https://www.youtube.com/watch?v=rw6veY7oKEM)

- [Digital twin for framework](https://www.baumaschinendienst.de/artikel/bim-model-garantiert-zuegige-bauausfuehrung-12654/)


---

## Industry feedback

- Karina Schiefer (PORR)
> "Great idea, but still needs work and improvment."

- Markus Netzker (ZechBau)
> "We can phone in this regard, I would like to support you."

---

## Thank you for your attention 

